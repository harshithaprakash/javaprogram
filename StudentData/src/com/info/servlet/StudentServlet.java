package com.info.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.info.model.Student;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Student> studList = new ArrayList<Student>();
		Student stud1 = new Student();
		stud1.setRollNo(1);
		stud1.setName("Eve");
		stud1.setSurName("Jackson");
		stud1.setMarks(94);
		
		Student stud2 = new Student();
		stud2.setRollNo(2);
		stud2.setName("John");
		stud2.setSurName("Doe");
		stud2.setMarks(80);
		
		Student stud3 = new Student();
		stud3.setRollNo(3);
		stud3.setName("Adam");
		stud3.setSurName("Johnson");
		stud3.setMarks(67);
		
		Student stud4 = new Student();
		stud4.setRollNo(4);
		stud4.setName("Jill");
		stud4.setSurName("Smith");
		stud4.setMarks(50);
		
		Student stud5 = new Student();
		stud5.setRollNo(5);
		stud5.setName("Jack");
		stud5.setSurName("Willson");
		stud5.setMarks(85);
		
		studList.add(stud1);
		studList.add(stud2);
		studList.add(stud3);
		studList.add(stud4);
		studList.add(stud5);
		
		request.setAttribute("studList", studList);
		request.setAttribute("htmlTagData", "</br>");
		request.setAttribute("url", "http://www.info.com");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/list.jsp");
		rd.forward(request, response);
		
		
	}
	

}
