import java.util.Scanner;
        import java.util.*;

class Ebill
{
    public static void main (String args[])
    {
        CustomerData cd = new CustomerData();
        cd.Getdata();
        cd.Calc();
        cd.Display();

    }
}

class CustomerData
{
    Scanner in = new Scanner (System.in);
    Scanner ins = new Scanner (System.in);
    String cname,fname;
    int bn;
    double units,tbill;

    void Getdata()
    {
        System.out.print ("Enter block number = ");
        bn = in.nextInt();
        System.out.print ("Enter flate name = ");
        fname = ins.nextLine();
        System.out.print ("Enter customer name = ");
        cname = ins.nextLine();
        System.out.print ("Enter total consumed units = ");
        units = in.nextDouble();
    }

    void Calc()
    {
        if (units<100)
            tbill=0.40*units;
        else if (units>100 && units<300)
            tbill=0.50*units;
        else
            tbill=0.60*units;
        if (tbill>150)
            tbill+=tbill*0.15;
    }

    void Display()
    {
        System.out.println ("\n\t Customer name = "+cname);
        System.out.println ("\n\t Total units = "+units);
        System.out.println ("\n\t Total bill =  "+tbill);
    }
}


