<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix= "c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student List</title>

<style>
table,th,td
{
	border:2px solid black;
}</style>
</head>
<body>
<table>
	<tbody>
		<tr><th>Roll No</th><th>Name</th><th>SurName</th><th>Marks</th></tr>

		<c:forEach items = "${requestScope.studList}" var = "stud">
		<tr><td><c:out value = "${stud.rollNo}"></c:out></td>
		<td><c:out value = "${stud.name}"></c:out></td>
		<td><c:out value = "${stud.surName}"></c:out></td>
		<td><c:out value = "${stud.marks}"></c:out></td>
		</tr></c:forEach>
		</tbody>
		</table>
		<br><br>
		
				</body>

</html>