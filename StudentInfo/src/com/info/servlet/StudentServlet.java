package com.info.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.info.model.Student;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class StudentServlet
 */
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		List<Student> studList = new ArrayList<Student>();
		Student stud1 = new Student();
		stud1.setRollNo(1);
		stud1.setName("Eve");
		stud1.setSurName("Jackson");
		stud1.setMarks(94);
		
		Student stud2 = new Student();
		stud2.setRollNo(2);
		stud2.setName("John");
		stud2.setSurName("Doe");
		stud2.setMarks(80);
		
		Student stud3 = new Student();
		stud3.setRollNo(3);
		stud3.setName("Adam");
		stud3.setSurName("Johnson");
		stud3.setMarks(67);
		
		Student stud4 = new Student();
		stud4.setRollNo(4);
		stud4.setName("Jill");
		stud4.setSurName("Smith");
		stud4.setMarks(50);
		
		Student stud5 = new Student();
		stud5.setRollNo(5);
		stud5.setName("Jack");
		stud5.setSurName("Willson");
		stud5.setMarks(85);
		
		Student stud6 = new Student();
		stud6.setRollNo(6);
		stud6.setName("David");
		stud6.setSurName("Miller");
		stud6.setMarks(92.5);
		
		studList.add(stud1);
		studList.add(stud2);
		studList.add(stud3);
		studList.add(stud4);
		studList.add(stud5);
		studList.add(stud6);
		request.setAttribute("studList", studList);
		
//response.sendRedirect("list.jsp");
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/list.jsp");         
		rd.forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	

}
